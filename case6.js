const getAngkaTerbesarKedua = (dataNumbers) => {
  const tipeData = typeof dataNumbers; //save type data of params
  // check if params object
  if (tipeData === typeof [3]) {
    // get max number from object
    const max = Math.max(...dataNumbers);
    let result = 0; //to store temporary 2nd greater value
    for (let i = 0; i < dataNumbers.length; i++) {
      // if value more than result and less than max so its 2nd greater
      if (dataNumbers[i] < max && dataNumbers[i] >= result) {
        result = dataNumbers[i];
      }
    }
    return result;
  } else if (tipeData === typeof "string" || tipeData === typeof 3) {
    //check if data type is string or number
    return "Error: hanya menerima tipe data objek";
  } else {
    //check is not empty params
    return "Error: pastikan params diisi";
  }
};
const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];
console.log(getAngkaTerbesarKedua(dataAngka));

console.log(getAngkaTerbesarKedua(0));

console.log(getAngkaTerbesarKedua());
