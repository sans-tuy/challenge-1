function changeWord(selectedText, changeText, text) {
  let caseSensitive = new RegExp(selectedText, "i"); //regEx for case sensitive
  //   select case sensitive text and change to lower case
  let lower = text.replace(caseSensitive, selectedText.toLowerCase()); //change text to lower
  let caseGlobal = new RegExp(selectedText.toLowerCase(), "g"); //regEx for global case
  //   change text to lower and selectedText with caseGlobal
  let newText = lower.replace(caseGlobal, changeText); //replace string
  return newText;
}

const kalimat1 = "Andini sangat mencintai kamu selamanya";
const kalimat2 =
  "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";

console.log(changeWord("mencintai", "membenci", kalimat1));
console.log(changeWord("bromo", "semeru", kalimat2));

// console.log(
//   changeWord(
//     "sapi",
//     "kambing",
//     "saya suka memakan olahan daging Sapi seperti gulai sapi"
//   )
// );
