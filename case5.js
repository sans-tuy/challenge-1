const resultSplitName = (personName) => {
  if (personName) {
    let nameElements = personName.split(/\s+/); //split text after whitespace
    //check has enough length ?
    let hasValidLength = nameElements.length > 0 && nameElements.length <= 3;
    let splittedName = {}; //store value after splitted

    if (hasValidLength) {
      let hasMiddleName = nameElements.length > 2; //length more than 2 has middle name
      let hasLastName = nameElements.length > 1; //length more than 1 has last name

      splittedName = {
        firstName: nameElements[0] ?? null, //assign first name
        middleName: hasMiddleName ? nameElements[1] : null, //assign middle name
        //assign last name
        lastName:
          hasLastName && hasMiddleName
            ? nameElements[2] //if length name 2
            : hasLastName
            ? nameElements[1] //if length name only 1
            : null,
      };
    } else {
      return "this funtion is only for 1 - 3 character name";
    }
    return splittedName;
  }
};

const getSplitName = (personName) => {
  const tipeData = typeof personName; //save type data of params
  // check if params string
  if (tipeData === "string") {
    return resultSplitName(personName);
  } else if (personName === undefined) {
    //check is no params
    return "Error: masukkan nama terlebih dahulu";
  } else {
    //check is wrong params
    return "Error: Hanya menerima params string";
  }
};
console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));
console.log(getSplitName());
