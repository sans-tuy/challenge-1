const isValidPassword = (pass) => {
  const tipeData = typeof pass; //save type data of params
  // check if params string
  if (tipeData === "string") {
    //regex pass format valid
    let passFormat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
    if (pass.match(passFormat)) {
      //if valid pass
      return true;
    } else {
      // regex pass format invalid
      let passformat = [
        /^(?=.*\d)/,
        /^(?=.*[a-z])/,
        /^(?=.*[A-Z])/,
        /^.{8,20}$/,
      ];
      return !pass.match(passformat[0])
        ? false
        : !pass.match(passformat[1])
        ? false
        : !pass.match(passformat[2])
        ? false
        : !pass.match(passformat[3])
        ? false
        : false;
    }
  } else if (pass === undefined) {
    //check is no params
    return "Error: Password belum diisi";
  } else {
    //check is wrong params
    return "Error: password harus berupa string";
  }
};
console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());
console.log(isValidPassword({}));
