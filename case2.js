const checkTypeNumber = (givenNumber) => {
  const tipeData = typeof givenNumber; //save type data of params
  // check if params string or number
  if (
    tipeData !== typeof "string" &&
    tipeData !== typeof [] &&
    givenNumber !== undefined
  ) {
    // check is even number or odd number
    return givenNumber % 2 == 0 ? "GENAP" : "GANJIL";
  } else if (givenNumber === undefined) {
    //check is no params
    return "Error: Bro where is the parameter";
  } else {
    //check is wrong params
    return "Error: Invalid data type";
  }
};
console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());
